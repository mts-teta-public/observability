package main

import (
	"context"
	"log"
	"net/http"
	"time"

	"github.com/TheZeroSlave/zapsentry"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/chi/v5"
	"github.com/juju/zaputil/zapctx"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/riandyrn/otelchi"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/exporters/jaeger"
	"go.opentelemetry.io/otel/propagation"
	"go.opentelemetry.io/otel/sdk/resource"
	tracesdk "go.opentelemetry.io/otel/sdk/trace"
	semconv "go.opentelemetry.io/otel/semconv/v1.4.0"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"moul.io/chizap"
)

const (
	DSN         = "https://94c0444acd3e4507a29b5e9b4c59403d@o1428566.ingest.sentry.io/6778904"
	TRACER_NAME = "demo_service"
)

var tracer = otel.Tracer(TRACER_NAME)

func initOtel() error {
	exp, err := jaeger.New(jaeger.WithCollectorEndpoint(
		jaeger.WithEndpoint("http://jaeger-instance-collector.observability:14268/api/traces")),
	)
	if err != nil {
		return err
	}

	tp := tracesdk.NewTracerProvider(
		tracesdk.WithBatcher(exp),
		tracesdk.WithResource(resource.NewWithAttributes(
			semconv.SchemaURL,
			semconv.ServiceNameKey.String("demo_service_name"),
		)))

	otel.SetTracerProvider(tp)
	otel.SetTextMapPropagator(propagation.TraceContext{})
	return nil
}

func initSentry(log *zap.Logger, sentryAddress, environment string) *zap.Logger {
	if sentryAddress == "" {
		return log
	}

	cfg := zapsentry.Configuration{
		Level: zapcore.ErrorLevel,
		Tags: map[string]string{
			"environment": environment,
			"app":         "demoApp",
		},
	}

	core, err := zapsentry.NewCore(cfg, zapsentry.NewSentryClientFromDSN(sentryAddress))
	if err != nil {
		log.Warn("failed to init zap", zap.Error(err))
	}

	return zapsentry.AttachCoreToLogger(core, log)
}

func getLogger(debug bool, dsn string, env string) (*zap.Logger, error) {
	var err error
	var l *zap.Logger

	if debug {
		l, err = zap.NewDevelopment()
	} else {
		l, err = zap.NewProduction()
	}

	l = initSentry(l, dsn, env)
	if err != nil {
		return nil, err
	}

	defer func() {
		_ = l.Sync()
	}()
	l.Debug("logger initialized in debug level")

	return l, err
}

func someFunc(ctx context.Context) {
	logger := zapctx.Logger(ctx)
	logger.Info("Logger from context")
}

func someFunc1(ctx context.Context) (string, error) {
	newCtx, span := tracer.Start(ctx, "Func1")
	defer span.End()
	time.Sleep(time.Second)
	err := someFunc2(newCtx)
	time.Sleep(time.Second)
	return "IDDQD", err
}

func someFunc2(ctx context.Context) error {
	newCtx, span := tracer.Start(ctx, "Func2")
	span.SetAttributes(attribute.KeyValue{Key: "func_name", Value: attribute.StringValue("someFunc2")})
	defer span.End()

	req, err := http.NewRequestWithContext(ctx, "GET", "http://127.0.0.1:3000/remoteservice", nil)
	if err != nil {
		return err
	}

	otel.GetTextMapPropagator().Inject(newCtx, propagation.HeaderCarrier(req.Header))
	http.DefaultClient.Do(req)

	return nil
}

func main() {
	r := chi.NewRouter()

	logger, err := getLogger(false, DSN, "myenv")
	logger = logger.With(zap.Any("someKey", "someValue"))
	if err != nil {
		log.Fatal(err)
	}
	logger.Error("Start!")

	if err := initOtel(); err != nil {
		logger.Fatal("OTEL init", zap.Error(err))
	}

	ctx := zapctx.WithLogger(context.Background(), logger)
	someFunc(ctx)

	r.Use(middleware.RequestID)
	r.Use(chizap.New(logger, &chizap.Opts{
		WithReferer:   true,
		WithUserAgent: true,
	}))
	r.Use(otelchi.Middleware("demo_service", otelchi.WithChiRoutes(r)))

	counter := promauto.NewCounter(prometheus.CounterOpts{
		Namespace: "team0", Name: "testcounter", Help: "Main endpoint request counter",
	})

	r.Get("/", func(w http.ResponseWriter, r *http.Request) {
		counter.Inc()
		_, err := w.Write([]byte("welcome"))
		if err != nil {
			logger.Error("error writing response", zap.Error(err))
		}
	})

	r.Get("/tracers", func(w http.ResponseWriter, r *http.Request) {
		newCtx, span := tracer.Start(r.Context(), "Main")
		text, err := someFunc1(newCtx)
		if err != nil {
			logger.Error("Tracers handler", zap.Error(err))
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte("this is error"))
		} else {
			w.WriteHeader(http.StatusOK)
			w.Write([]byte(text))
		}
		span.End()
	})

	r.Get("/remoteservice", func(w http.ResponseWriter, r *http.Request) {
		ctx := otel.GetTextMapPropagator().Extract(r.Context(), propagation.HeaderCarrier(r.Header))
		_, span := tracer.Start(ctx, "remoteservice")
		defer span.End()
		span.AddEvent("Something happen")
		w.WriteHeader(http.StatusOK)
		w.Write([]byte("OK"))
	})

	http.Handle("/metrics", promhttp.Handler())
	go http.ListenAndServe(":9000", nil)
	http.ListenAndServe(":3000", r)

}
